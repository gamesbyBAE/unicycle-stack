using UnityEngine;

[CreateAssetMenu(fileName = "New Int Value", menuName = "Data Types/Int Value")]
public class IntVariableSO : ScriptableObject
{
    [SerializeField] int intValue;

    public int IntValue() => intValue;
}
