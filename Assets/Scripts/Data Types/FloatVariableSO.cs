using UnityEngine;

/// <summary>
/// This class is used to create a SO to be used as a float data container.
/// </summary>
[CreateAssetMenu(fileName = "New Float Value", menuName = "Data Types/Float Value", order = 51)]
public class FloatVariableSO : ScriptableObject
{
    [SerializeField] float floatValue;

    // Equivalent to returning value using getter.
    public float FloatValue() => floatValue; 
}
