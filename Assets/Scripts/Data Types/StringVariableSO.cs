using UnityEngine;

/// <summary>
/// This class is used to create a SO to be used as a string data container.
/// </summary>
[CreateAssetMenu(fileName = "New String Value", menuName = "Data Types/String Value")]
public class StringVariableSO : ScriptableObject
{
    [SerializeField] string stringValue;

    public string StringValue() => stringValue;
}
