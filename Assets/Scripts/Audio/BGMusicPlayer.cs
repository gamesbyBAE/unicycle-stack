using UnityEngine;

public class BGMusicPlayer : MonoBehaviour
{
    private static BGMusicPlayer bgMusicInstance;
    void Awake()
    {
        if(bgMusicInstance == null)
        {
            bgMusicInstance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

}
