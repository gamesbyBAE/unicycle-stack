using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used for Events that has no argument.
/// Example: To send message of game over, level failed, etc.
/// </summary>
[CreateAssetMenu(fileName = "New Void Event Channel", menuName = "Events/Void Event Channel")]
public class VoidEventChannelSO : ScriptableObject
{
    public UnityAction OnVoidEventRaised;

    public void RaiseEvent()
    {
        OnVoidEventRaised?.Invoke();
    }
}
