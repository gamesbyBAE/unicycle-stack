using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used for Events that have one int argument.
/// </summary>
[CreateAssetMenu(fileName = "New Int Event Channel", menuName = "Events/Int Event Channel")]
public class IntEventChannelSO : ScriptableObject
{
    public UnityAction<int> OnIntValueRaised;

    public void RaiseEvent(int val)
    {
        OnIntValueRaised?.Invoke(val);
    }
}
