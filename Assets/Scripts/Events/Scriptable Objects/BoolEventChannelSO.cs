using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is used for Events that have one bool argument.
/// Example: To relay the message Pickup Wheels GO collided with Player.
/// </summary>

[CreateAssetMenu(fileName = "New Bool Event Channel", menuName = "Events/Bool Event Channel", order = 52)]
public class BoolEventChannelSO : ScriptableObject
{
    public UnityAction<bool> OnBoolValueRaised;

    public void RaiseEvent(bool val)
    {
        OnBoolValueRaised?.Invoke(val);
    }
}
