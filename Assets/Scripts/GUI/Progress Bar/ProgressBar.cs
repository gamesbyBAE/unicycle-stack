using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Fills the progress bar as the Unicycle keeps moving forward towards the Finish GameObject.
/// </summary>
public class ProgressBar : MonoBehaviour
{
    [SerializeField] GameObject playerGO;
    [SerializeField] GameObject finishGO;

    Image filledBar;
    float totalDistance;

    void Start()
    {
        totalDistance = finishGO.transform.position.z;
        filledBar = GetComponent<Image>();

        filledBar.fillAmount = playerGO.transform.position.z / totalDistance;
    }

    private void Update()
    {
        // FillAmount can have values between 0 and 1 only.
        if (filledBar.fillAmount < 1)
        {
            filledBar.fillAmount = playerGO.transform.position.z / totalDistance;
        }
    }
}
