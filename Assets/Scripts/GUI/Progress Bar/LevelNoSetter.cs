using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

/// <summary>
/// Dynamically sets the Current Level Number and Next Level Number
/// in progress bar.
/// </summary>
public class LevelNoSetter : MonoBehaviour
{
    [Tooltip("Number of Scenes in Build Settings before Level 1 scene.")]
    [SerializeField] IntVariableSO levelStartOffset = default;
 
    [SerializeField] TextMeshProUGUI currentLvlText = default;
    [SerializeField] TextMeshProUGUI nextLvlText = default;

    void Awake()
    {
        int currentLevel = SceneManager.GetActiveScene().buildIndex - levelStartOffset.IntValue() + 1;
        currentLvlText.text = currentLevel.ToString();

        nextLvlText.text = currentLevel < (SceneManager.sceneCountInBuildSettings - levelStartOffset.IntValue()) ? (currentLevel+1).ToString() : "x";
    }
}
