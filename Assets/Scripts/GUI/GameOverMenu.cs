using UnityEngine;

/// <summary>
/// Sets active various Game Over menus depending on the state,
/// like, Continue menu, Level Failed menu, Level Completed menu
/// and Pause menu.
/// </summary>
public class GameOverMenu : MonoBehaviour
{
    [SerializeField] GameObject levelFailedGO = default;
    [SerializeField] GameObject levelFinishedGO = default;

    [Header("Events to raise.")]
    [SerializeField] VoidEventChannelSO gameStarted = default;

    [Header("Events to subscribe.")]
    [SerializeField] VoidEventChannelSO levelFailedEvent = default; // Need to replace it with Level Failed event, raised when player decides to NOT continue.
    [SerializeField] VoidEventChannelSO levelFinishedEvent = default;

    private void Start()
    {
        levelFailedGO.SetActive(false);
        levelFinishedGO.SetActive(false);
        gameStarted.RaiseEvent();
    }

    private void OnEnable()
    {
        levelFailedEvent.OnVoidEventRaised += ShowLevelFailMenu;
        levelFinishedEvent.OnVoidEventRaised += ShowLevelFinishedMenu;
    }
    private void OnDisable()
    {
        levelFailedEvent.OnVoidEventRaised -= ShowLevelFailMenu;
        levelFinishedEvent.OnVoidEventRaised -= ShowLevelFinishedMenu;
    }

    void ShowLevelFailMenu()
    {
        levelFailedGO.SetActive(true);
    }

    void ShowLevelFinishedMenu()
    {
        levelFinishedGO.SetActive(true);
    }
}
