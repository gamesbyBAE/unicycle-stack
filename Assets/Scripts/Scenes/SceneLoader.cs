using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Loads various scenes according to raised events. Avoiding usage of Singleton.
/// This works in Editor only, where it works according to the raised events.
/// 
/// To make it work in Builds, add the SO Asset file to
/// Project Settings -> Player -> Other Settings -> Optimization -> Preloaded Assets
/// Unity will load such assets in runtime before scene loading and will keep them alive
/// until the game/app has been terminated.
/// 
/// But this works only in Builds in Editor. But since we are using events as well, it works
/// in Editor as well without need of additional scripts to run it in Editor for testing purposes.
/// </summary>
[CreateAssetMenu(fileName = "Scene Loader SO", menuName = "Scene Loader")]
public class SceneLoader : ScriptableObject
{
    [Header("Scene Names")]
    [SerializeField] string homeSceneName = default;
    [SerializeField] string levelsSceneName = default;

    [Header("Events to Subscribe")]
    [SerializeField] VoidEventChannelSO playGameEvent = default;
    [SerializeField] VoidEventChannelSO homeSceneEvent = default;
    [SerializeField] VoidEventChannelSO restartLevelEvent = default;
    [SerializeField] VoidEventChannelSO nextLevelEvent = default;
    [SerializeField] VoidEventChannelSO levelsSceneEvent = default;
    [SerializeField] IntEventChannelSO loadLevelNoEvent = default;

    private void OnEnable()
    {
        playGameEvent.OnVoidEventRaised += PlayGame;
        homeSceneEvent.OnVoidEventRaised += HomeScene;
        restartLevelEvent.OnVoidEventRaised += RestartLevel;
        nextLevelEvent.OnVoidEventRaised += NextLevel;
        levelsSceneEvent.OnVoidEventRaised += LevelsScene;
        loadLevelNoEvent.OnIntValueRaised += LoadLevel;
    }
    private void OnDisable()
    {
        playGameEvent.OnVoidEventRaised -= PlayGame;
        homeSceneEvent.OnVoidEventRaised -= HomeScene;
        restartLevelEvent.OnVoidEventRaised -= RestartLevel;
        nextLevelEvent.OnVoidEventRaised -= NextLevel;
        levelsSceneEvent.OnVoidEventRaised -= LevelsScene;
        loadLevelNoEvent.OnIntValueRaised -= LoadLevel;
    }

    // Loads the latest unlocked level.
    // Currently loads Lvl 1.
    void PlayGame()
    {
        LoadLevel(1);
    }
    void HomeScene()
    {
        SceneManager.LoadScene(homeSceneName);
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void LevelsScene()
    {
        SceneManager.LoadScene(levelsSceneName);
    }

    void LoadLevel(int number)
    {
        SceneManager.LoadScene(string.Concat("Level ",number.ToString()));
    }
}
