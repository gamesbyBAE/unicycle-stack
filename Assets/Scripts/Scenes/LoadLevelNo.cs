using UnityEngine;

public class LoadLevelNo : MonoBehaviour
{
    [SerializeField] int levelNumber = default;
    
    [Header("Event to Raise")]
    [SerializeField] IntEventChannelSO levelNumberEvent = default;

    private void Start()
    {
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(RequestLevel);
    }

    void RequestLevel()
    {
        levelNumberEvent.RaiseEvent(levelNumber);
    }
}
