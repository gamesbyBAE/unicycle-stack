using System.Collections;
using UnityEngine;


/// <summary>
/// Shows "Double Tap to Exit" message on pressing Back key once.
/// If pressed within given time, the game quits.
/// Else hides the message.
/// Also hides, if user touches somewhere on the screen.
/// </summary>
public class QuitGame : MonoBehaviour
{
    [Tooltip("Time limit player has to press Back key again before the message disappears in order to quit.")]
    [SerializeField] float waitTime = default;


    [Header("Events to Subscribe")]
    [SerializeField] VoidEventChannelSO backKeyPressedEvent = default;
    [SerializeField] VoidEventChannelSO touchedScreen = default;

    bool backKeyPressedOnce;
    Canvas exitGUI;

    IEnumerator coroutine;
    WaitForSeconds waitForSeconds;

    private void Start()
    {
        backKeyPressedOnce = false;

        exitGUI = GetComponentInChildren<Canvas>();
        exitGUI.enabled = false;

        coroutine = null;
        waitForSeconds = new WaitForSeconds(waitTime);

        backKeyPressedEvent.OnVoidEventRaised += Quit;
        touchedScreen.OnVoidEventRaised += CancelQuit;
    }

    private void OnDestroy()
    {
        backKeyPressedEvent.OnVoidEventRaised -= Quit;
        touchedScreen.OnVoidEventRaised -= CancelQuit;
    }

    void Quit()
    {
        // Back key pressed for the first time/one time.
        if (!backKeyPressedOnce)
        {
            backKeyPressedOnce = true;
            exitGUI.enabled = true; //Animate using LeanTween. 

            coroutine = WaitTime();
            StartCoroutine(coroutine);
        }

        // Back key pressed again within time limit.
        else
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
            Application.Quit();
        }
    }

    IEnumerator WaitTime()
    {
        yield return waitForSeconds;

        CancelQuit();
    }

    // Hides message when time limit has passed
    // or user touched anywhere on the screen.
    void CancelQuit()
    {
        if (backKeyPressedOnce)
        {
            backKeyPressedOnce = false;
            exitGUI.enabled = false; //Animate using LeanTween.
            StopCoroutine(coroutine);
            coroutine = null;
        }
    }
}
