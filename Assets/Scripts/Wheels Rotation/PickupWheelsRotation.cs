using UnityEngine;

/// <summary>
/// This class inherits from the WheelsRotation class.
/// The parent game object collides with the player then only the children wheels of Pickup Wheels GO
/// are allowed to rotate, not before that.
/// 
/// But since all of the Pickup Wheels game objects are subscribed to the same event, it is important
/// to know the ID of the game object who raised the event, so that only it's children start rotating
/// and not every Pickup Wheels' children.
/// </summary>
public class PickupWheelsRotation : WheelsRotation
{
    [Tooltip("Listening to Instance ID of the GO who raised the event in CollidedWithPlayer class.")]
    [SerializeField] IntEventChannelSO parentIDChannel = default;
    [SerializeField] VoidEventChannelSO gameOverEvent = default;

    private void OnEnable()
    {
        parentIDChannel.OnIntValueRaised += DidParentRaise;
        gameOverEvent.OnVoidEventRaised += OnBecameInvisible;
    }

    private void OnDisable()
    {
        parentIDChannel.OnIntValueRaised -= DidParentRaise;
        gameOverEvent.OnVoidEventRaised -= OnBecameInvisible;
    }

    // Start rotating the wheel.
    void DidParentRaise(int val)
    {
        if (val == this.transform.parent.GetInstanceID())
        {
            RotateWheel(true);
        }
    }

    // Stop rotating the wheel/game object when out of camera's view.
    private void OnBecameInvisible()
    {
        RotateWheel(false);
    }
}