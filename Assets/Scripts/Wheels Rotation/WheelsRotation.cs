using UnityEngine;

/// <summary>
/// Base class that handles rotation of a wheel.
/// </summary>
public class WheelsRotation : MonoBehaviour
{
    [SerializeField] FloatVariableSO rotationSpeed = default;
    [SerializeField] bool reverseRotation = false;
    
    float xVal;
    bool rotate;

    private void Awake()
    {
        rotate = false;
        xVal = (reverseRotation ? -1 : 1) * rotationSpeed.FloatValue();
    }
    
    void Update()
    {
        if (rotate)
        {
            this.transform.Rotate(new Vector3(xVal, 0, 0) * Time.deltaTime);
        }
    }

    // 'protected' means that it is visible only inside this class and classes derived from it.
    // 'virtual' means that it can be overriden in derived classes.
    protected void RotateWheel(bool val)
    {
        rotate = val;
    }
}
