using UnityEngine;

/// <summary>
/// This class handles rotation of the Unicycle wheel.
/// Derives from the WheelsRotation class.
/// </summary>
public class UnicycleWheelRotation : WheelsRotation
{
    [Header("Events to subscribe.")]
    [SerializeField] VoidEventChannelSO gameOverEvent = default;

    private void Start()
    {
        RotateWheel(true);
    }

    private void OnEnable()
    {
        gameOverEvent.OnVoidEventRaised += StopRotation;

        // TO-DO: Subscribe Continue event that calls Continued method.
    }
    private void OnDisable()
    {
        gameOverEvent.OnVoidEventRaised -= StopRotation;

        // TO-DO: Unsubscribe Continue event.
    }

    // Stop rotation when game overs.
    void StopRotation()
    {
        RotateWheel(false);
    }

    // After watching Ad, when player continues.
    void Continued()
    {
        RotateWheel(true);
    }
}
