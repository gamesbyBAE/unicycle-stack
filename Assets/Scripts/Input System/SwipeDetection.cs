using UnityEngine;

/// <summary>
/// This classes subscribes to events of InputReader SO and calculates
/// from the position and time whether the user swiped left or right.
/// </summary>
public class SwipeDetection : MonoBehaviour
{
    // Reference to InputReader SO for to subscribe to it's events.
    [SerializeField] InputReader inputReader = default;

    // Raises the event with the int value of 1 or -1 according to the swipe direction.
    [Header("Event to raise.")]
    [SerializeField] IntEventChannelSO swipeDirectionChannel = default; 

    [Header("Threshold Values for Swipe Detection")]
    [Tooltip("Resultant value should be greater or equal than it.")]
    [SerializeField] float minDistance = default;

    [Tooltip("Resultant value should be lesser or equal than it.")]
    [SerializeField] float maxDuration = default;

    [Tooltip("Resultant value should be greater or equal than it.")]
    [SerializeField] [Range(0,1)] float directionAccuracy = default;


    Camera mainCamera;

    Vector2 touchStartPosition, touchEndPosition;
    float touchStartTime, touchEndTime;

    void Start()
    {
        mainCamera = Camera.main;
    }

    private void OnEnable()
    {
        inputReader.startTouchEvent += SwipeStart;
        inputReader.endTouchEvent += SwipeEnd;
    }

    private void OnDisable()
    {
        inputReader.startTouchEvent -= SwipeStart;
        inputReader.endTouchEvent -= SwipeEnd;
    }

    void SwipeStart(Vector2 position, float time)
    {
        touchStartPosition = MyScreenToWorldPointConverter(position);
        touchStartTime = time;
    }

    void SwipeEnd(Vector2 postion, float time)
    {
        touchEndPosition = MyScreenToWorldPointConverter(postion);
        touchEndTime = time;
        SwipeDirection();
    }

    void SwipeDirection()
    {
        if ((Vector3.Distance(touchStartPosition,touchEndPosition) >= minDistance) && ((touchEndTime-touchStartTime) <= maxDuration))
        {
            // Extracting only the direction.
            Vector2 dist = (touchEndPosition - touchStartPosition).normalized; 

            // Swiped Left
            if (Vector2.Dot(Vector2.left, dist) > directionAccuracy)
            {
                swipeDirectionChannel.RaiseEvent(-1);
            }

            // Swiped Right
            if (Vector2.Dot(Vector2.right, dist) > directionAccuracy)
            {
                swipeDirectionChannel.RaiseEvent(1);
            }
        }
    }

    Vector3 MyScreenToWorldPointConverter(Vector3 pos)
    {
        // Keeps on returning the position of the camera if "nearClipPlane" is not used.
        return mainCamera.ScreenToWorldPoint(new Vector3(pos.x, pos.y, mainCamera.nearClipPlane)); 
    }
}
