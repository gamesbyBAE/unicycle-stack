using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

/// <summary>
/// This class detects user inputs using the NEW Input system.
/// Raises events when user starts and stops touching the screen.
/// </summary>

// ITouchActionMapActions is an interface generated from the "TouchActionMap" action map we added.
// This was triggered by the "Generate Interfaces" checkbox.
public class InputReader : MonoBehaviour, PlayerInput.ITouchActionMapActions, PlayerInput.IExitActionMapActions
{
    public UnityAction<Vector2, float> startTouchEvent;
    public UnityAction<Vector2, float> endTouchEvent;

    [Header("Events to Subscribe")]
    [SerializeField] VoidEventChannelSO gameOverEvent = default;
    [SerializeField] VoidEventChannelSO startedLevel = default;

    [Header("Events to Raise")]
    [SerializeField] VoidEventChannelSO backKeyPressed = default;
    [SerializeField] VoidEventChannelSO touchedScreen = default;


    // PlayerInput is the C# class that Unity generated.
    // It encapsulates the data from the .inputactions asset we created
    // and automatically looks up all the maps and actions for us.
    PlayerInput playerInput;

    private void OnEnable()
    {
        if (playerInput == null)
        {
            playerInput = new PlayerInput();

            // Tell the action maps that we want to get told about when actions of them get triggered.
            playerInput.TouchActionMap.SetCallbacks(this);
            playerInput.ExitActionMap.SetCallbacks(this);
        }
        playerInput.Enable();
        //SwitchActionMapToUI();

        gameOverEvent.OnVoidEventRaised += SwitchActionMapToUI;
        startedLevel.OnVoidEventRaised += SwitchActionMapToTouch;
    }

    private void OnDisable()
    {
        playerInput.Disable();

        gameOverEvent.OnVoidEventRaised -= SwitchActionMapToUI;
        startedLevel.OnVoidEventRaised -= SwitchActionMapToTouch;
    }

    // Switches Action Map from Touch to UI when game overs.
    void SwitchActionMapToUI()
    {
        playerInput.UIActionMap.Enable();
        playerInput.TouchActionMap.Disable();
    }

    // Switches Action Map from UI to Touch when game starts.
    void SwitchActionMapToTouch()
    {
        playerInput.UIActionMap.Disable();
        playerInput.TouchActionMap.Enable();
    }

    // Method declared inside the "ITouchActionMapActions" interface and named
    // automatically according to it's Action name, i.e., PrimaryContact.
    public void OnPrimaryContact(InputAction.CallbackContext context)
    {
        // Starts touching the screen.
        if (context.phase == InputActionPhase.Started)
        {
            startTouchEvent?.Invoke(playerInput.TouchActionMap.PrimaryPosition.ReadValue<Vector2>(), (float)context.startTime);

            touchedScreen.RaiseEvent(); // Raising primarily for QuitGame class.
        }

        // Stops touching the screen.
        if (context.phase == InputActionPhase.Canceled)
        {
            endTouchEvent?.Invoke(playerInput.TouchActionMap.PrimaryPosition.ReadValue<Vector2>(), (float)context.startTime);
        }
    }

    // Method declared inside the "ITouchActionMapActions" interface and named
    // automatically according to it's Action name, i.e., PrimaryPosition.
    // Don't need to do anything in this method but have to write because of Interface.
    public void OnPrimaryPosition(InputAction.CallbackContext context)
    {

    }


    // Method declared in "IExitActionMapActions" interface.
    // When Back key is pressed.
    public void OnBackButtonAction(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            backKeyPressed.RaiseEvent();
        }
    }
}
