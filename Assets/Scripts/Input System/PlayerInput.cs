// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input System/PlayerInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInput"",
    ""maps"": [
        {
            ""name"": ""TouchActionMap"",
            ""id"": ""e0e3a0d2-794a-4f4a-a644-3cb5f7ba02ae"",
            ""actions"": [
                {
                    ""name"": ""PrimaryContact"",
                    ""type"": ""Button"",
                    ""id"": ""30752a4b-f92f-426c-b313-7d31a48221f2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PrimaryPosition"",
                    ""type"": ""Value"",
                    ""id"": ""3c5b0d00-aa54-4202-8453-102887de77a7"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0ca5dc02-8136-4545-b753-4a2b24a1eb6b"",
                    ""path"": ""<Touchscreen>/primaryTouch/press"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryContact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9e57ac2d-66b1-40a6-b9a0-893484a41ba0"",
                    ""path"": ""<Touchscreen>/primaryTouch/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PrimaryPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UIActionMap"",
            ""id"": ""9b542a9b-27fa-4675-ae53-0e817ec110c0"",
            ""actions"": [
                {
                    ""name"": ""Click"",
                    ""type"": ""PassThrough"",
                    ""id"": ""6fd950d7-1733-42ad-9213-85f838cca944"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Point"",
                    ""type"": ""PassThrough"",
                    ""id"": ""386969e9-392c-452d-a1a0-d18284350722"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c6d0e469-a4a3-4e1c-aa72-a2529516517f"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Point"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6260ce36-c57a-456f-aea8-8922ca4e9345"",
                    ""path"": ""<Pen>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard&Mouse"",
                    ""action"": ""Point"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""45a3c07a-b2ac-4023-867c-15051441c962"",
                    ""path"": ""<Touchscreen>/touch*/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Touch"",
                    ""action"": ""Point"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee0c9324-e9af-4dae-ba8a-043f3dc042a7"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard&Mouse"",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b035617b-f5a7-4c9c-8511-b45ea69dc005"",
                    ""path"": ""<Pen>/tip"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard&Mouse"",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""58bfda3e-295b-4bb0-8f65-77ab72f29a5f"",
                    ""path"": ""<Touchscreen>/touch*/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Touch"",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3ff69957-df84-45e9-a4b4-558149ed482c"",
                    ""path"": ""<XRController>/trigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""XR"",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""ExitActionMap"",
            ""id"": ""ab740751-63ed-472e-8238-bcef2dc3459b"",
            ""actions"": [
                {
                    ""name"": ""BackButtonAction"",
                    ""type"": ""Button"",
                    ""id"": ""6d0f3f22-b7e0-4297-96a2-b905dd40d405"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""84051d24-5fe1-4151-9ace-77c530c0dd59"",
                    ""path"": ""*/{Back}"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""BackButtonAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // TouchActionMap
        m_TouchActionMap = asset.FindActionMap("TouchActionMap", throwIfNotFound: true);
        m_TouchActionMap_PrimaryContact = m_TouchActionMap.FindAction("PrimaryContact", throwIfNotFound: true);
        m_TouchActionMap_PrimaryPosition = m_TouchActionMap.FindAction("PrimaryPosition", throwIfNotFound: true);
        // UIActionMap
        m_UIActionMap = asset.FindActionMap("UIActionMap", throwIfNotFound: true);
        m_UIActionMap_Click = m_UIActionMap.FindAction("Click", throwIfNotFound: true);
        m_UIActionMap_Point = m_UIActionMap.FindAction("Point", throwIfNotFound: true);
        // ExitActionMap
        m_ExitActionMap = asset.FindActionMap("ExitActionMap", throwIfNotFound: true);
        m_ExitActionMap_BackButtonAction = m_ExitActionMap.FindAction("BackButtonAction", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // TouchActionMap
    private readonly InputActionMap m_TouchActionMap;
    private ITouchActionMapActions m_TouchActionMapActionsCallbackInterface;
    private readonly InputAction m_TouchActionMap_PrimaryContact;
    private readonly InputAction m_TouchActionMap_PrimaryPosition;
    public struct TouchActionMapActions
    {
        private @PlayerInput m_Wrapper;
        public TouchActionMapActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @PrimaryContact => m_Wrapper.m_TouchActionMap_PrimaryContact;
        public InputAction @PrimaryPosition => m_Wrapper.m_TouchActionMap_PrimaryPosition;
        public InputActionMap Get() { return m_Wrapper.m_TouchActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(TouchActionMapActions set) { return set.Get(); }
        public void SetCallbacks(ITouchActionMapActions instance)
        {
            if (m_Wrapper.m_TouchActionMapActionsCallbackInterface != null)
            {
                @PrimaryContact.started -= m_Wrapper.m_TouchActionMapActionsCallbackInterface.OnPrimaryContact;
                @PrimaryContact.performed -= m_Wrapper.m_TouchActionMapActionsCallbackInterface.OnPrimaryContact;
                @PrimaryContact.canceled -= m_Wrapper.m_TouchActionMapActionsCallbackInterface.OnPrimaryContact;
                @PrimaryPosition.started -= m_Wrapper.m_TouchActionMapActionsCallbackInterface.OnPrimaryPosition;
                @PrimaryPosition.performed -= m_Wrapper.m_TouchActionMapActionsCallbackInterface.OnPrimaryPosition;
                @PrimaryPosition.canceled -= m_Wrapper.m_TouchActionMapActionsCallbackInterface.OnPrimaryPosition;
            }
            m_Wrapper.m_TouchActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PrimaryContact.started += instance.OnPrimaryContact;
                @PrimaryContact.performed += instance.OnPrimaryContact;
                @PrimaryContact.canceled += instance.OnPrimaryContact;
                @PrimaryPosition.started += instance.OnPrimaryPosition;
                @PrimaryPosition.performed += instance.OnPrimaryPosition;
                @PrimaryPosition.canceled += instance.OnPrimaryPosition;
            }
        }
    }
    public TouchActionMapActions @TouchActionMap => new TouchActionMapActions(this);

    // UIActionMap
    private readonly InputActionMap m_UIActionMap;
    private IUIActionMapActions m_UIActionMapActionsCallbackInterface;
    private readonly InputAction m_UIActionMap_Click;
    private readonly InputAction m_UIActionMap_Point;
    public struct UIActionMapActions
    {
        private @PlayerInput m_Wrapper;
        public UIActionMapActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Click => m_Wrapper.m_UIActionMap_Click;
        public InputAction @Point => m_Wrapper.m_UIActionMap_Point;
        public InputActionMap Get() { return m_Wrapper.m_UIActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIActionMapActions set) { return set.Get(); }
        public void SetCallbacks(IUIActionMapActions instance)
        {
            if (m_Wrapper.m_UIActionMapActionsCallbackInterface != null)
            {
                @Click.started -= m_Wrapper.m_UIActionMapActionsCallbackInterface.OnClick;
                @Click.performed -= m_Wrapper.m_UIActionMapActionsCallbackInterface.OnClick;
                @Click.canceled -= m_Wrapper.m_UIActionMapActionsCallbackInterface.OnClick;
                @Point.started -= m_Wrapper.m_UIActionMapActionsCallbackInterface.OnPoint;
                @Point.performed -= m_Wrapper.m_UIActionMapActionsCallbackInterface.OnPoint;
                @Point.canceled -= m_Wrapper.m_UIActionMapActionsCallbackInterface.OnPoint;
            }
            m_Wrapper.m_UIActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Click.started += instance.OnClick;
                @Click.performed += instance.OnClick;
                @Click.canceled += instance.OnClick;
                @Point.started += instance.OnPoint;
                @Point.performed += instance.OnPoint;
                @Point.canceled += instance.OnPoint;
            }
        }
    }
    public UIActionMapActions @UIActionMap => new UIActionMapActions(this);

    // ExitActionMap
    private readonly InputActionMap m_ExitActionMap;
    private IExitActionMapActions m_ExitActionMapActionsCallbackInterface;
    private readonly InputAction m_ExitActionMap_BackButtonAction;
    public struct ExitActionMapActions
    {
        private @PlayerInput m_Wrapper;
        public ExitActionMapActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @BackButtonAction => m_Wrapper.m_ExitActionMap_BackButtonAction;
        public InputActionMap Get() { return m_Wrapper.m_ExitActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ExitActionMapActions set) { return set.Get(); }
        public void SetCallbacks(IExitActionMapActions instance)
        {
            if (m_Wrapper.m_ExitActionMapActionsCallbackInterface != null)
            {
                @BackButtonAction.started -= m_Wrapper.m_ExitActionMapActionsCallbackInterface.OnBackButtonAction;
                @BackButtonAction.performed -= m_Wrapper.m_ExitActionMapActionsCallbackInterface.OnBackButtonAction;
                @BackButtonAction.canceled -= m_Wrapper.m_ExitActionMapActionsCallbackInterface.OnBackButtonAction;
            }
            m_Wrapper.m_ExitActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @BackButtonAction.started += instance.OnBackButtonAction;
                @BackButtonAction.performed += instance.OnBackButtonAction;
                @BackButtonAction.canceled += instance.OnBackButtonAction;
            }
        }
    }
    public ExitActionMapActions @ExitActionMap => new ExitActionMapActions(this);
    public interface ITouchActionMapActions
    {
        void OnPrimaryContact(InputAction.CallbackContext context);
        void OnPrimaryPosition(InputAction.CallbackContext context);
    }
    public interface IUIActionMapActions
    {
        void OnClick(InputAction.CallbackContext context);
        void OnPoint(InputAction.CallbackContext context);
    }
    public interface IExitActionMapActions
    {
        void OnBackButtonAction(InputAction.CallbackContext context);
    }
}
