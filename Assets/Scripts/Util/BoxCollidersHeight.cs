using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Util class to calculate the Height of the Box Collider(s).
/// Primarily used to figure out height of the Pick Up Wheels prefab.
/// </summary>
public class BoxCollidersHeight : MonoBehaviour
{
   private void Start()
    {
        float height = 0f;
        foreach (BoxCollider colls in GetComponents<BoxCollider>())
        {
            height += colls.bounds.size.y;
        }
        Debug.Log("Height of Bounding Box: " + height);
    }
}
