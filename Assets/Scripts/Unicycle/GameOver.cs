using System;
using UnityEngine;

/// <summary>
/// Raises a game over event when obstacles collide with player.
/// </summary>
public class GameOver : MonoBehaviour
{
    [SerializeField] StringVariableSO unicycleTag = default;

    [Header("Event to raise.")]
    [SerializeField] VoidEventChannelSO gameOverEvent = default;
    [SerializeField] VoidEventChannelSO levelFailedEvent = default;

    private void OnCollisionEnter(Collision collision)
    {
        // Using Math.Round to get float value upto 2 decimal places as
        // relativeVelocity frequently returned values more than 7 decimal places.
        if (collision.collider.gameObject.CompareTag(unicycleTag.StringValue()) && Math.Round(collision.relativeVelocity.y,2) == 0f)
        {
            // Disabling gravity of Unicycle so it doesn't glitches when level fails with Pickup Wheels under it.
            collision.collider.GetComponentInParent<Rigidbody>().useGravity = false;

            gameOverEvent.RaiseEvent();
            levelFailedEvent.RaiseEvent();
        }
    }
}
