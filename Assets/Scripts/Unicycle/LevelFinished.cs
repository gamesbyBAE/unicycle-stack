using UnityEngine;

/// <summary>
/// Raises Level Finished Event when Unicycle crosses the Finish Line.
/// </summary>
public class LevelFinished : MonoBehaviour
{
    [SerializeField] StringVariableSO playerTag = default;
    [SerializeField] StringVariableSO pickupWheelTage = default;

    [Header("Event to raise.")]
    [SerializeField] VoidEventChannelSO levelFinishedEvent = default;
    [SerializeField] VoidEventChannelSO gameOverEvent = default;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag.StringValue()) || other.CompareTag(pickupWheelTage.StringValue()))
        {
            levelFinishedEvent.RaiseEvent();
            gameOverEvent.RaiseEvent();
        }
    }
}
