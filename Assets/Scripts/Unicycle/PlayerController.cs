using UnityEngine;

/// <summary>
/// Handles the movement of the Unicycle.
/// </summary>
public class PlayerController : MonoBehaviour
{
    //[SerializeField] float forwardSpeed = default;
    [SerializeField] float forwardSpeed = default;
    [SerializeField] float laneChangeSpeed = default;
    [SerializeField] float myGravity = default;

    [Header("Event Channels to Subscribe")]
    [Tooltip("Listening to the value of Swipe Direction raised by the SwipeDetection class.")]
    [SerializeField] IntEventChannelSO swipeDirection = default;
    [SerializeField] FloatEventChannelSO upValue = default;
    [SerializeField] VoidEventChannelSO gameOverEvent = default;

    Rigidbody rb;
    Vector3 targetPos;
    int targetLane;
    bool changeLane, stopMotion;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        targetPos = Vector3.zero;
        Physics.gravity = new Vector3(0,-myGravity,0);
        stopMotion = false;
        changeLane = false;
    }

    private void OnEnable()
    {
        swipeDirection.OnIntValueRaised += DirectionValue;
        upValue.OnFloatValueRaised += MoveUp;
        gameOverEvent.OnVoidEventRaised += StopMotion;
    }

    private void OnDisable()
    {
        swipeDirection.OnIntValueRaised -= DirectionValue;
        upValue.OnFloatValueRaised -= MoveUp;
        gameOverEvent.OnVoidEventRaised -= StopMotion;
    }

    void FixedUpdate()
    {
        if (!stopMotion)
        {
            MoveForward();
        }

        // 'changeLane' is set to true whenever an event is raised by the 
        // SwipeDetection class and meets the condition in DirectionValue method(below).
        if (changeLane)
        {
            MoveSide();
        }

        rb.MovePosition(targetPos);
    }

    void MoveForward()
    {
        targetPos.z = rb.transform.position.z;
        targetPos.y = rb.transform.position.y;
        targetPos.z += 1 * forwardSpeed * Time.fixedDeltaTime;
    }

    // Smoothly changing the lane till the Unicycle GO reaches the target position.
    void MoveSide()
    {
        if (Mathf.Abs(targetLane-targetPos.x) <= 0)
        {
            changeLane = false;
        }
        targetPos.x = Mathf.MoveTowards(rb.transform.position.x, targetLane, laneChangeSpeed * Time.fixedDeltaTime);
    }

    // Making the Unicycle move up and at the top of Pickup Wheels on collision with it.
    void MoveUp(float upVal)
    {
        rb.transform.position = new Vector3(rb.transform.position.x, rb.transform.position.y + upVal, rb.transform.position.z);
    }

    void StopMotion()
    {
        targetPos.z = rb.transform.position.z;
        stopMotion = true;
    }

    void DirectionValue(int direction)
    {
        targetLane = Mathf.RoundToInt(rb.transform.position.x);
        targetLane += direction;

        // Unicycle GO isn't allowed to go out of x-coordinate bounds for lane change.
        // The x-coordinate of the Unicycle's position can either be -1, 0 or 1.
        if (targetLane >= -1 && targetLane <= 1)
        {
            changeLane = true;
        }
    }
}
