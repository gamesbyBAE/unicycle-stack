using UnityEngine;

/// <summary>
/// Controls the placement of the Pickup Wheels below the Unicycle or other Pickup Wheels.
/// Adjusts the y position of the stacked Unicycle with the collection of every new Pickup Wheels.
/// </summary>
public class StackHandler : MonoBehaviour
{
    [SerializeField] StringVariableSO pickupWheelsTag = default;

    [Tooltip("Float value by which Unicycle has to be shifted up.")]
    [SerializeField] FloatVariableSO shiftUpBy = default;

    [Header("Event Channel")]
    [SerializeField] FloatEventChannelSO moveUnicycleUp = default;

    private void OnCollisionEnter(Collision collision)
    {
        // Checks if the collided GO is not it's child already, since the child Pickup Wheels also
        // keeps on colliding with the parent Unicycle.
        if (collision.gameObject.CompareTag(pickupWheelsTag.StringValue()) && !collision.gameObject.transform.IsChildOf(this.gameObject.transform))
        {
            // Making the Unicycle move up.
            // PlayerController class subscribes to this event.
            moveUnicycleUp.RaiseEvent(shiftUpBy.FloatValue());

            // Setting Unicycle as the parent of the Pickup Wheel.
            collision.gameObject.transform.parent = this.gameObject.transform;
            collision.gameObject.transform.localPosition = new Vector3(0, collision.gameObject.transform.localPosition.y, 0);
        }
    }
}
