using UnityEngine;

/// <summary>
/// This class raises event with Instance ID of the Pickup Wheels
/// that collides with the player so that the StackHandler class can
/// place/stack it accordingly.
/// </summary>
public class CollidedWithPlayer : MonoBehaviour
{
    [SerializeField] StringVariableSO unicycleTag = default;

    [Header("Events to Raise")]
    [SerializeField] IntEventChannelSO pickUpWheelID = default;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(unicycleTag.StringValue()))
        {
            pickUpWheelID.RaiseEvent(this.gameObject.transform.GetInstanceID());
        }
    }
}
