using UnityEngine;

/// <summary>
/// It unparents the Pickup Wheels game objects if:
///     1. The collided game object(GO) has the tag "PickupWheels"
///     2. Collides only from the front side and not from top.
///     
/// Attaching this script on Obstacles prefab as child Pick Up Wheels prefab
/// is a Static Collider(w/o Rigidbody) and according to the Collision Matrix
/// (https://docs.unity3d.com/Manual/CollidersOverview.html),
/// Kinematic Collider detects Rigidbody Collider which Unicyle has.
/// Pick Up Wheels prefab don't have Rigidbody because it reacts with Unicycle's
/// Rigidbody.
/// </summary>
public class UnchildWheel : MonoBehaviour
{
    [SerializeField] StringVariableSO pickupWheelsTag = default;

    private void OnCollisionEnter(Collision collision)
    {
        // "collision.collider.gameObject" : returns the actual collided game object, i.e, the Pickup Wheels.
        // "collision.gameObject"          : returns the parent game object, i.e, the Unicycle.

        // RelativeVelocity of Pick Up wheels touching top of box is <= -1f.

        GameObject go = collision.collider.gameObject;
        if (go.CompareTag(pickupWheelsTag.StringValue()) && collision.relativeVelocity.y >= -0.5f)
        {
            go.transform.parent = null;
            go.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
